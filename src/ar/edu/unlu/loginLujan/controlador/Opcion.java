package ar.edu.unlu.loginLujan.controlador;

public enum Opcion {
	SALIR, 
	MENU_CREAR_USUARIO, 
	MOSTRAR_USUARIO, 
	MENU_LOGIN, 
	LOGOUT, 
	MOSTRAR_USUARIO_LOGUEADO,
	USUARIO_PASSWORD_INGRESADOS,
	CREAR_USUARIO,
	LOGIN
}
