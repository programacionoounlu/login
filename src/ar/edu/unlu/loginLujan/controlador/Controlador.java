package ar.edu.unlu.loginLujan.controlador;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Scanner;

import ar.edu.unlu.loginLujan.IObservador;
import ar.edu.unlu.loginLujan.Pair;
import ar.edu.unlu.loginLujan.modelo.CambiosModelo;
import ar.edu.unlu.loginLujan.modelo.GestorCuentas;
import ar.edu.unlu.loginLujan.modelo.IModelo;
import ar.edu.unlu.loginLujan.modelo.Login;
import ar.edu.unlu.loginLujan.modelo.Mostrable;
import ar.edu.unlu.loginLujan.modelo.TransformadorFuerte;
import ar.edu.unlu.loginLujan.modelo.UsuarioOPasswordIncorrecto;
import ar.edu.unlu.loginLujan.vista.IVista;
import ar.edu.unlu.loginLujan.vista.consola.MenuConsola;
import ar.edu.unlu.rmimvc.cliente.IControladorRemoto;
import ar.edu.unlu.rmimvc.observer.IObservableRemoto;

public class Controlador implements IObservador, IControladorRemoto, Serializable { 
	
	private IVista vista;
	private IModelo modelo;
	private boolean logueando;
	
	public Controlador(IVista vista) {
		this.vista = vista;
		vista.agregarObservador(this);
	}
	
	//public void setModelo(IModelo modelo) {
	//	this.modelo = modelo;
	//}

	@Override
	public void update(Opcion opcion, Object data) {
		Pair<String,String> userYPass;
		switch(opcion) {
		case MENU_CREAR_USUARIO:
			vista.crearUsuario();
			break;
		case CREAR_USUARIO:					
			userYPass = (Pair<String, String>) data;
			try {
				modelo.agregarUsuario(userYPass.getKey(), userYPass.getValue());
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
			vista.mostrarVista();
			break;
		case MOSTRAR_USUARIO:
			Mostrable[] usuarios;
			try {
				usuarios = modelo.obtenerUsuarios();
				vista.mostrarUsuarios(usuarios);
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			vista.mostrarVista();
			break;
		case LOGIN:
			userYPass = (Pair<String, String>) data;
			try {
				System.out.println("Controlador: llamando a login del modelo..");
				modelo.login(userYPass.getKey(), userYPass.getValue());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			vista.mostrarVista();
			break;
		case MENU_LOGIN:
			vista.loginUsuario();
			break;
		case LOGOUT:
			boolean deslogeado;
			try {
				modelo.logout();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			vista.mostrarVista();
			break;
		case MOSTRAR_USUARIO_LOGUEADO:
			Mostrable usuarioLogueado;
			try {
				usuarioLogueado = modelo.getUsuarioLogeado();
				vista.actualizarUsuarioLogeado(usuarioLogueado);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			vista.mostrarVista();
			break;
		case SALIR:
			System.exit(0);
			break;
		default:
			vista.mostrarError("La opcion es inválida");
			vista.mostrarVista();
	}
	}

	@Override
	public void actualizar(IObservableRemoto arg0, Object cambio) throws RemoteException {
		switch((CambiosModelo) cambio) {
		case CAMBIO_USUARIO_LOGUEADO:
			vista.actualizarUsuarioLogeado(modelo.getUsuarioLogeado());
			break;
		case CAMBIO_USUARIOS:
			vista.mostrarUsuarios(modelo.obtenerUsuarios());
			break;
		case LOGIN_CORRECTO:
			vista.mostrarResultadoLogin(true);
			break;
		case LOGIN_INCORRECTO:
			vista.mostrarResultadoLogin(false);
			break;
		case LOGOUT_CORRECTO:
			vista.mostrarResultadoLogout(true);
			break;
		}
	}

	@Override
	public <T extends IObservableRemoto> void setModeloRemoto(T arg0) throws RemoteException {
		this.modelo = (IModelo) arg0;
	}

}
