package ar.edu.unlu.loginLujan.modelo;

import java.io.Serializable;
import java.rmi.RemoteException;

import ar.edu.unlu.rmimvc.observer.ObservableRemoto;

public class Modelo extends ObservableRemoto implements IModelo, Serializable {
	private Login login;
	private GestorCuentas gestorCuentas;
	
	public Modelo() {
		gestorCuentas = GestorCuentas.getInstancia(new TransformadorFuerte());
		login = new Login();
	}
	
	@Override
	public void login(String usuario, String password) throws RemoteException {
		try {
			System.out.println("Logueando..");
			this.login.login(usuario, password);
			System.out.println("Login realizado");
			notificarObservadores(CambiosModelo.LOGIN_CORRECTO);
			System.out.println("Notificado Login Correcto");
			notificarObservadores(CambiosModelo.CAMBIO_USUARIO_LOGUEADO);
			System.out.println("Notificado usuario logueado");
		} catch (UsuarioOPasswordIncorrecto e) {
			notificarObservadores(CambiosModelo.LOGIN_INCORRECTO);
		}
	}
	
	@Override
	public void logout()  throws RemoteException {
		if (this.login.logout()) {
			notificarObservadores(CambiosModelo.LOGOUT_CORRECTO);
		}else {
			notificarObservadores(CambiosModelo.LOGOUT_INCORRECTO);
		}
		
	}
	
	@Override
	public Mostrable getUsuarioLogeado() throws RemoteException {
		return this.login.getUsuarioLogeado();
	}
	
	@Override
	public void agregarUsuario(String usuario, String pass) throws RemoteException {
		this.gestorCuentas.agregarUsuario(usuario, pass);
		notificarObservadores(CambiosModelo.CAMBIO_USUARIOS);
	}
	
	@Override
	public Usuario buscarUsuario(String usuario, String pass) throws RemoteException {
		return this.gestorCuentas.buscarUsuario(usuario, pass);
	}


	@Override
	public Mostrable[] obtenerUsuarios() throws RemoteException {
		return this.gestorCuentas.obtenerUsuarios();
		
	}
}
