package ar.edu.unlu.loginLujan.modelo;

public class TransformadorDebil implements Transformable {

	private final String A = "a";
	private final String F = "f";
	
	@Override
	public String transformarPassword(String pass) {

		String  nuevoPassword = "";
		for (int i = 0 ; i < 5; i++) {
			nuevoPassword += A;
		}
		nuevoPassword += pass;
		for (int i = 0 ; i < 10; i++) {
			nuevoPassword += F;
		}
		return nuevoPassword;
	}

}
