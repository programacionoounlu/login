package ar.edu.unlu.loginLujan.modelo;

public class Login {
	private Usuario actual;
	private GestorCuentas gestorCuentas;
	
	public Login(){
		gestorCuentas = GestorCuentas.getInstancia(null);
	}
	
	public void login(String usuario, String password) throws UsuarioOPasswordIncorrecto {
		System.out.println("Intentando login");
		Usuario u = gestorCuentas.buscarUsuario(usuario, password);
		System.out.println("Usuario encontrado");
		if(u != null) {
			actual = u;
		}else {
			throw new UsuarioOPasswordIncorrecto();
		}
		System.out.println("Fin login");
	}
	
	public boolean logout() {
		actual = null;
		return true;
	}
	
	public Mostrable getUsuarioLogeado() {
		return actual;
	}
	
	
	
	
}
