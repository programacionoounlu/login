package ar.edu.unlu.loginLujan.modelo;

import java.io.Serializable;

public interface Transformable extends Serializable{
	public String transformarPassword(String pass);
}
