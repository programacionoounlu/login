package ar.edu.unlu.loginLujan.modelo;

import java.io.Serializable;

public class Usuario implements Mostrable, Serializable {
	private String pass;
	private String usuario;
	
	public Usuario(String usuario, String pass){
		this.usuario = usuario;
		setPass(pass);
	}
	
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	@Override
	public String getUsuario() {
		return usuario;
	}
	
	private void setPass(String pass) {
		this.pass = pass;
	}
	
	/**
	 * Este metodo devuelve la contaseña 
	 * encriptada del usuario.
	 * @return
	 */
	public String getPass() {
		return pass;
	}
	
	public boolean isMyPass(String pass){
		return pass == this.pass;
	}
	
	@Override
	public String toString() {
		return usuario;
	}
}
