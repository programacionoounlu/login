package ar.edu.unlu.loginLujan.modelo;

public class TransformadorFuerte implements Transformable {
	
	private final String ALGO = "p";
	@Override
	public String transformarPassword(String pass) {
		String  nuevoPassword = "";
		for(int i = 0;i<pass.length();i++) {
			nuevoPassword+=pass.charAt(i)+ALGO;
		}
		return nuevoPassword;
	}

}
