package ar.edu.unlu.loginLujan.modelo;

import java.io.Serializable;
import java.util.ArrayList;

public class GestorCuentas implements Serializable{
	private ArrayList<Usuario> usuarios;
	private static GestorCuentas instancia;
	
	private Transformable transformador;
	
	private GestorCuentas(Transformable t) {
		usuarios = new ArrayList<Usuario>();
		transformador = t;
	}
	
	
	public static GestorCuentas getInstancia(Transformable t) {
		if(t != null && instancia == null){
			instancia = new GestorCuentas(t);
		}
		return instancia;
	}
	
	public void agregarUsuario(String usuario, String pass) {
		String nuevoPassword = transformador.transformarPassword(pass);
		usuarios.add(new Usuario(usuario, nuevoPassword));
	}
	
	public Usuario buscarUsuario(String usuario, String pass) {
		Usuario res = null;
		boolean encontrado = false;
		int index = 0;
		while(!encontrado && index < this.usuarios.size()) {
			if(this.usuarios.get(index).getUsuario().equals(usuario)){
				res = this.usuarios.get(index);
				encontrado = true;
			}
			index++;
		}
		if(encontrado && res.getPass().equals(this.transformador.transformarPassword(pass)))
			return res;
		else
			return null;
	}


	public Mostrable[] obtenerUsuarios() {
		Mostrable[] res = new Mostrable[this.usuarios.size()];
	
		for(int index = 0;index < this.usuarios.size();index++) {
			res[index] = this.usuarios.get(index);
		}
		return res;
		
	}
}
