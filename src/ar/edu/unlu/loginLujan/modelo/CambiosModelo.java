package ar.edu.unlu.loginLujan.modelo;

public enum CambiosModelo {
	CAMBIO_USUARIOS,
	CAMBIO_USUARIO_LOGUEADO,
	LOGIN_CORRECTO,
	LOGIN_INCORRECTO,
	LOGOUT_CORRECTO,
	LOGOUT_INCORRECTO

	
}
