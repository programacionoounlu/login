package ar.edu.unlu.loginLujan.modelo;

import java.rmi.RemoteException;

import ar.edu.unlu.rmimvc.observer.IObservableRemoto;

public interface IModelo extends IObservableRemoto {

	void login(String usuario, String password) throws RemoteException;

	void logout() throws RemoteException;

	Mostrable getUsuarioLogeado() throws RemoteException;

	void agregarUsuario(String usuario, String pass) throws RemoteException;

	Usuario buscarUsuario(String usuario, String pass) throws RemoteException;

	Mostrable[] obtenerUsuarios() throws RemoteException;

}