package ar.edu.unlu.loginLujan;

import ar.edu.unlu.loginLujan.controlador.Opcion;

public interface IObservador {
	void update(Opcion opcion, Object data);
}
