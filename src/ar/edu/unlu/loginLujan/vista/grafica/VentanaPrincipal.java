package ar.edu.unlu.loginLujan.vista.grafica;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import ar.edu.unlu.loginLujan.modelo.Mostrable;

import com.jgoodies.forms.layout.FormSpecs;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JList;

public class VentanaPrincipal extends JFrame {

	private JPanel contentPane;
	private VistaGrafica vista;
	private JLabel lblUsuarioActual;
	private JButton btnLogin;
	private JButton btnLogout;
	private JScrollPane scrollPane;
	private JList<Mostrable> lstUsuarios;
	private DefaultListModel<Mostrable> lstModelo;


	/**
	 * Create the frame.
	 */
	public VentanaPrincipal(VistaGrafica vista) {
		this.vista = vista;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,}));
		
		JButton btnCrearUsuario = new JButton("Crear usuario");
		btnCrearUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vista.menuCrearUsuario();
			}
		});
		contentPane.add(btnCrearUsuario, "2, 2");
		
		JLabel lblUsuarioActual_ = new JLabel("Usuario actual:");
		contentPane.add(lblUsuarioActual_, "10, 2");
		
		lblUsuarioActual = new JLabel("");
		contentPane.add(lblUsuarioActual, "12, 2");
		
		btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vista.menuLogin();
			}
		});
		contentPane.add(btnLogin, "2, 4");
		
		btnLogout = new JButton("Logout");
		btnLogout.setEnabled(false);
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vista.logout();
			}
		});
		
		scrollPane = new JScrollPane();
		contentPane.add(scrollPane, "6, 4, 5, 3, fill, fill");
		
		lstUsuarios = new JList<>();
		lstModelo = new DefaultListModel<>();
		lstUsuarios.setModel(lstModelo);
		scrollPane.setViewportView(lstUsuarios);
		contentPane.add(btnLogout, "2, 6");
	}


	public void mostrarUsuarioLogueado(Mostrable usuarioLogueado) {
		if (usuarioLogueado == null) {
			lblUsuarioActual.setText("No usuario logueado");
		} else {
			lblUsuarioActual.setText(usuarioLogueado.getUsuario());
		}
	}


	public void habilitarBotonLogout(boolean b) {
		btnLogout.setEnabled(b);
	}
	
	public void habilitarBotonLogin(boolean b) {
		btnLogin.setEnabled(b);
	}
	
	public void mostrarUsuarios(Mostrable[] usuarios) {
		lstModelo.removeAllElements();
		for (Mostrable mostrable: usuarios) {
			lstModelo.addElement(mostrable);
		}
	}

}
