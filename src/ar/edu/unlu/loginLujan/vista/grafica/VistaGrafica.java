package ar.edu.unlu.loginLujan.vista.grafica;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import ar.edu.unlu.loginLujan.IObservador;
import ar.edu.unlu.loginLujan.Pair;
import ar.edu.unlu.loginLujan.controlador.Opcion;
import ar.edu.unlu.loginLujan.modelo.Mostrable;
import ar.edu.unlu.loginLujan.vista.IVista;

public class VistaGrafica implements IVista {
	private VentanaLogin ventanaLogin;
	private VentanaPrincipal ventanaPrincipal;
	
	private List<IObservador> observadores;
	private VentanaCrearUsuario ventanaCrearUsuario;
	
	public VistaGrafica() {
		observadores = new ArrayList<>();
		this.ventanaPrincipal = new VentanaPrincipal(this);
		this.ventanaLogin = new VentanaLogin(this.ventanaPrincipal, this);
		this.ventanaCrearUsuario = new VentanaCrearUsuario(this.ventanaPrincipal, this);
	}
	
	public void menuLogin() {
		notificar(Opcion.MENU_LOGIN);
	}
	
	public void menuCrearUsuario() {
		notificar(Opcion.MENU_CREAR_USUARIO);
	}
	
	public void login(String usuario, String pass) {
		notificar(Opcion.LOGIN, 
				new Pair<String, String>(usuario,pass));
	}
	
	public void logout() {
		notificar(Opcion.LOGOUT);
	}
	
	public void crearUsuario(String usuario, String pass) {
		notificar(Opcion.CREAR_USUARIO, 
				new Pair<String, String>(usuario,pass));
	}

	@Override
	public void mostrarVista() {
		this.ventanaPrincipal.setVisible(true);
		this.ventanaLogin.setVisible(false);
		this.ventanaCrearUsuario.setVisible(false);
	}

	@Override
	public void mostrarError(String mensaje) {
		//JOptionPane.showMessageDialog(ventanaPrincipal, mensaje);
		System.out.println("ERROR: " + mensaje);
	}

	@Override
	public void crearUsuario() {
		this.ventanaCrearUsuario.setVisible(true);
	}

	@Override
	public void loginUsuario() {
		this.ventanaLogin.setVisible(true);
	}

	@Override
	public void mostrarUsuarios(Mostrable[] usuarios) {
		this.ventanaPrincipal.mostrarUsuarios(usuarios);
	}

	@Override
	public void mostrarResultadoLogin(boolean success) {
		if (success) {
			//JOptionPane.showMessageDialog(ventanaLogin, "Inicio de sesi�n correcto.");
			System.out.println("Inicio de sesi�n correcto.");
			this.ventanaPrincipal.habilitarBotonLogout(true);
			this.ventanaPrincipal.habilitarBotonLogin(false);
		} else {
			//JOptionPane.showMessageDialog(ventanaLogin, "Inicio de sesi�n incorrecto.");
			System.out.println("Inicio de sesi�n incorrecto.");
		}
	}

	@Override
	public void mostrarResultadoLogout(boolean success) {
		if (success) {
			//JOptionPane.showMessageDialog(ventanaLogin, "Logout correcto.");
			System.out.println("Logout correcto.");
			this.ventanaPrincipal.mostrarUsuarioLogueado(null);
			this.ventanaPrincipal.habilitarBotonLogout(false);
			this.ventanaPrincipal.habilitarBotonLogin(true);
		} else {
			//JOptionPane.showMessageDialog(ventanaLogin, "Logout incorrecto.");
			System.out.println("Logout incorrecto.");
		}
	}

	@Override
	public void actualizarUsuarioLogeado(Mostrable usuarioLogueado) {
		System.out.println("Mostrando usuario logueado");
		this.ventanaPrincipal.mostrarUsuarioLogueado(usuarioLogueado);
	}

	@Override
	public void iniciar() {
		this.mostrarVista();
	}

	@Override
	public void agregarObservador(IObservador observador) {
		observadores.add(observador);
	}
	
	private void notificar(Opcion opcion) {
		notificar(opcion, null);
	}
	
	private void notificar(Opcion opcion, Object data) {
		for (IObservador observador: observadores) {
			observador.update(opcion, data);
		}
	}
	
	
}
