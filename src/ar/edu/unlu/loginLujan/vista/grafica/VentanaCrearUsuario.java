package ar.edu.unlu.loginLujan.vista.grafica;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaCrearUsuario extends JDialog {

	private JPanel contentPane;
	private JTextField txtUsuario;
	private JLabel lblPassword;
	private JTextField txtPassword;
	private JButton btnCrearUsuario;
	private VistaGrafica vista;



	/**
	 * Create the frame.
	 */
	public VentanaCrearUsuario(JFrame parent, VistaGrafica vista) {
		super(parent);
		this.vista = vista;
		
		setModal(true);
		setResizable(false);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 584, 178);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,}));
		
		JLabel lblUsuario = new JLabel("Usuario");
		contentPane.add(lblUsuario, "2, 2, right, default");
		
		txtUsuario = new JTextField();
		contentPane.add(txtUsuario, "4, 2, fill, default");
		txtUsuario.setColumns(10);
		
		lblPassword = new JLabel("Password");
		contentPane.add(lblPassword, "2, 4, right, default");
		
		txtPassword = new JTextField();
		contentPane.add(txtPassword, "4, 4, fill, default");
		txtPassword.setColumns(10);
		
		btnCrearUsuario = new JButton("Crear usuario");
		btnCrearUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vista.crearUsuario(txtUsuario.getText(), txtPassword.getText());
			}
		});
		contentPane.add(btnCrearUsuario, "4, 6, right, default");
	}

}
