package ar.edu.unlu.loginLujan.vista;

import ar.edu.unlu.loginLujan.IObservador;
import ar.edu.unlu.loginLujan.modelo.Mostrable;

public interface IVista {
	
	public static final String[] opcionesTexto =  {"Salir", "Crear usuario", "Mostrar usuario", "Login", "Logout", "Mostrar usuario logeado"};
	
	void mostrarVista();
	void mostrarError(String mensaje);
	
	void crearUsuario();
	void loginUsuario();
	
	void mostrarUsuarios(Mostrable[] usuarios);
	
	void mostrarResultadoLogin(boolean success);
	
	void mostrarResultadoLogout(boolean success);
	
	void actualizarUsuarioLogeado(Mostrable usuarioLogueado);
	
	void iniciar();
	
	void agregarObservador(IObservador observador);
}
