package ar.edu.unlu.loginLujan.vista.consola;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import ar.edu.unlu.loginLujan.IObservador;
import ar.edu.unlu.loginLujan.Pair;
import ar.edu.unlu.loginLujan.controlador.Opcion;
import ar.edu.unlu.loginLujan.modelo.Mostrable;
import ar.edu.unlu.loginLujan.vista.IVista;

public class MenuConsola implements IVista{
	Scanner in;
	
	private List<IObservador> observadores;
	
	
	public MenuConsola() {
		in = new Scanner(System.in);
		observadores = new ArrayList<>();
	}
	
	@Override
	public void mostrarVista() {
		System.out.println(Opcion.SALIR.ordinal()+" - Salir");
		System.out.println(Opcion.MENU_CREAR_USUARIO.ordinal()+" - Crear usuario");
		System.out.println(Opcion.MOSTRAR_USUARIO.ordinal()+" - Mostrar usuario");
		System.out.println(Opcion.MENU_LOGIN.ordinal()+" - Login");
		System.out.println(Opcion.LOGOUT.ordinal()+" - Logout");
		System.out.println(Opcion.MOSTRAR_USUARIO_LOGUEADO.ordinal()+" - Mostrar usuario logeado");
		Opcion value = Opcion.values()[in.nextInt()];
		//Limpio el buffer
		in.nextLine();
		notificar(value);
	}
	
	public void crearUsuario() {
		System.out.println("Ingrese usuario:");
		String usuario = in.nextLine();
		System.out.println("Ingrese contraseña:");
		String pass = in.nextLine();
		//String pass = new String(System.console().readPassword());
		//return new Pair<String, String>(usuario,pass);
		notificar(Opcion.CREAR_USUARIO, 
				new Pair<String, String>(usuario,pass));
	}
	
	public void loginUsuario() {
		System.out.println("Ingrese usuario:");
		String usuario = in.nextLine();
		System.out.println("Ingrese contraseña:");
		String pass = in.nextLine();
		//String pass = new String(System.console().readPassword());
		//return new Pair<String, String>(usuario,pass);
		notificar(Opcion.LOGIN, 
				new Pair<String, String>(usuario,pass));
	}
	
	

	@Override
	public void mostrarError(String mensaje) {
		System.err.println(mensaje);
	}

	@Override
	public void mostrarUsuarios(Mostrable[] usuarios) {
		System.out.println();
		System.out.println("Estos son los usuarios registrados:");
		for(Mostrable m : usuarios) {
			System.out.println("\t* "+m.getUsuario());
		}
		System.out.println();
	}

	@Override
	public void mostrarResultadoLogin(boolean success) {
		if(success) {
			System.out.println("Logeado!");
		}else {
			mostrarError("No se pudo logear! Verifique su clave y usuario.");
		}
		
	}

	@Override
	public void mostrarResultadoLogout(boolean success) {
		if(success) {
			System.out.println("Hasta luego!");
		}else {
			mostrarError("No se pudo logear! Verifique su clave y usuario.");
		}		
	}

	@Override
	public void actualizarUsuarioLogeado(Mostrable usuarioLogueado) {
		System.out.println();
		if(usuarioLogueado != null) {
			System.out.println("El usuario logueado es: "+usuarioLogueado.getUsuario());
		}else {
			System.out.println("Ningun usuario se encuentra logeado!");
		}
		System.out.println();
	}
	
	public void agregarObservador(IObservador observador) {
		observadores.add(observador);
	}
	
	private void notificar(Opcion opcion) {
		notificar(opcion, null);
	}
	
	private void notificar(Opcion opcion, Object data) {
		for (IObservador observador: observadores) {
			observador.update(opcion, data);
		}
	}
	
	public void iniciar() {
		mostrarVista();
	}

}
