package ar.edu.unlu.loginLujan;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import ar.edu.unlu.loginLujan.controlador.Controlador;
import ar.edu.unlu.loginLujan.modelo.GestorCuentas;
import ar.edu.unlu.loginLujan.modelo.IModelo;
import ar.edu.unlu.loginLujan.modelo.Modelo;
import ar.edu.unlu.loginLujan.modelo.Mostrable;
import ar.edu.unlu.loginLujan.modelo.TransformadorFuerte;
import ar.edu.unlu.loginLujan.vista.IVista;
import ar.edu.unlu.loginLujan.vista.consola.MenuConsola;
import ar.edu.unlu.loginLujan.vista.grafica.VistaGrafica;

public class Main {

	public static void main(String[] args) {
		GestorCuentas a = GestorCuentas.getInstancia(new TransformadorFuerte());
		a.agregarUsuario("andres", "1234");
		a.agregarUsuario("juan", "43212");
		a.agregarUsuario("fede", "1");
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream("GestorCuentas");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(a);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FileInputStream fin = null;
		GestorCuentas b = null;
		try {
			fin = new FileInputStream("GestorCuentas");
			ObjectInputStream oos = new ObjectInputStream(fin);
			b = (GestorCuentas) oos.readObject();
			for(Mostrable m : b.obtenerUsuarios()) {
				System.out.println(m.getUsuario());
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    }

}
